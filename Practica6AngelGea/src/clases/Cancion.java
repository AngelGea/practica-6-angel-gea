package clases;

import java.time.LocalDate;

public class Cancion {
private String nombre;
private String album;
private Double duracion;
private LocalDate fechaSalida;
private   Cantante cantanteCancion;

public Cancion (String nombre, String album, Double duracion) {
	this.nombre=nombre;
	this.album=album;
	this.duracion=duracion;
}


public String getNombre() {
	return nombre;
}
public void setNombre(String nombre) {
	this.nombre = nombre;
}
public String getAlbum() {
	return album;
}
public void setAlbum(String album) {
	this.album = album;
}
public Double getDuracion() {
	return duracion;
}
public void setDuracion(Double duracion) {
	this.duracion = duracion;
}
public LocalDate getFechaSalida() {
	return fechaSalida;
}
public void setFechaSalida(LocalDate fechaSalida) {
	this.fechaSalida = fechaSalida;
}
public Cantante getCantanteCancion() {
	return cantanteCancion;
}
public void setCantanteCancion(Cantante cantanteCancion) {
	this.cantanteCancion = cantanteCancion;
}
@Override
public String toString() {
	return "Cancion [nombre =" + nombre + ", album =" + album + ", duracion =" + duracion + ", fechaSalida =" + fechaSalida
			+ ", cantanteCancion=" + cantanteCancion + ", toString()=" + "]";
}



}
