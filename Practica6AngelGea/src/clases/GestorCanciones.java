package clases;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Iterator;

public class GestorCanciones {
	private ArrayList<Cancion> listaCanciones;
	private ArrayList<Cantante> listaCantantes;

public GestorCanciones() {
	listaCanciones = new ArrayList<Cancion>();
	listaCantantes = new ArrayList<Cantante>();
}


public void altaCantante (String nombre, String estilo ) {
	if (!existeCantante(nombre)) {
		Cantante nuevoCantante = new Cantante(nombre, estilo);
		nuevoCantante.setFechaConcierto(LocalDate.now());
		listaCantantes.add(nuevoCantante);
	} else {
		System.out.println("El cantante ya existe");
	}
}

public boolean existeCantante(String nombre) {
	for (Cantante cantante: listaCantantes) {
		if(cantante!=null && cantante.getNombre().contentEquals(nombre)) {
			return true;
		}
	}
	return false;
}

public void listarCantantes() {
	for (Cantante cantante: listaCantantes) {
		if(cantante !=null) {
			System.out.println(cantante);
		}
	}
}

public void listarCanciones() {
	for (Cancion cancion: listaCanciones) {
		if(cancion !=null) {
			System.out.println(cancion);
		}
	}
}

public Cantante buscarCantante (String nombre) {
	for (Cantante cantante : listaCantantes ) {
		if(cantante!=null && cantante.getNombre().contentEquals(nombre)) {
			return cantante;
		}
	}
	return null;
}

public void altaCancion (String nombre, String album, Double duracion, String fechaSalida) {
	Cancion nuevaCancion = new Cancion(nombre, album, duracion);
	nuevaCancion.setFechaSalida(LocalDate.parse(fechaSalida));
	listaCanciones.add(nuevaCancion);
}

 public void eliminarCancion (String nombre) {
	 Iterator<Cancion> iteratorCanciones = listaCanciones.iterator();
	 
	 while (iteratorCanciones.hasNext()) {
		 Cancion cancion = iteratorCanciones.next();
		 if (cancion.getNombre().contentEquals(nombre)) {
			 iteratorCanciones.remove();
		 }
	 }
 }
 public void asignarCantante(String nombre, String nombreCancion) {
	 if(buscarCantante(nombre)!=null && buscarCancion(nombreCancion)!=null) {
		 Cantante cantante = buscarCantante(nombre);
		 Cancion cancion = buscarCancion(nombreCancion);
		 cancion.setCantanteCancion(cantante);
	 }
 }
 public Cancion buscarCancion(String nombreCancion) {
	 for (Cancion cancion: listaCanciones) {
		 if(cancion !=null && cancion.getNombre().equals(nombreCancion)) {
			 return cancion;
		 }
	 }
	 return null;
 }
 public void listarCancionAnio (int anio) {
	 for (Cancion cancion: listaCanciones) {
		 if(cancion.getFechaSalida().getYear()==anio) {
			 System.out.println(cancion);
		 }
	 }
 }
 public void listarCancionesCantantes(String nombre) {
	 for(Cancion cancion: listaCanciones) {
		 if(cancion.getCantanteCancion()!=null && cancion.getCantanteCancion().getNombre().equals(nombre)){
			 System.out.println(cancion);
		 }
	 }
 }
 public void asignarCantanteExperto(String nombreCancion) {
	 Cancion cancion = buscarCancion(nombreCancion);
	 if(cancion !=null) {
		 cancion.setCantanteCancion(buscarCantanteExperto());
	 }
	 
 }
 
 public Cantante buscarCantanteExperto() {
	 LocalDate fechaAntigua=null;
	 for (int i=0; i<listaCantantes.size();i++) {
		 Cantante cantanteActual=listaCantantes.get(i);
		 if(cantanteActual!=null && i==0) {
			 fechaAntigua=cantanteActual.getFechaConcierto();
		 } else {
			 if (cantanteActual!=null && cantanteActual.getFechaConcierto().isAfter(fechaAntigua) ) {
				 fechaAntigua=cantanteActual.getFechaConcierto();
				 
			 }
		 }
	 }
	 for (Cantante cantante : listaCantantes) {
		 if(cantante!=null && cantante.getFechaConcierto().equals(fechaAntigua)) {
			 return cantante;
		 }
	 }
	 return null;
 }
 
 public void ere() {
	 Iterator<Cantante> iteratorCantantes = listaCantantes.iterator();
	 
	 while (iteratorCantantes.hasNext()) {
		 Cantante cantanteActual = iteratorCantantes.next();
		 boolean estaActuando = false;
		 for (Cancion cancion : listaCanciones) {
			 if (cancion.getCantanteCancion() != null && cancion.getCantanteCancion().getNombre().equals(cantanteActual.getNombre().equals(cantanteActual.getNombre()))) {
				 estaActuando=true;
			 }
	 }
	 if (!estaActuando) {
		 iteratorCantantes.remove();
	 }
 }
}
}
