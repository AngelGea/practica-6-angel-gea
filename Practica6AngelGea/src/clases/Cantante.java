package clases;

import java.time.LocalDate;

public class Cantante {

	private String nombre;
	
	private String cancion;
	
	private LocalDate fechaConcierto;
	
	
	public Cantante(String nombre, String cancion) {
		this.nombre=nombre;
		this.cancion=cancion;
	}


	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getCancion() {
		return cancion;
	}

	public void setCancion(String cancion) {
		this.cancion = cancion;
	}

	public LocalDate getFechaConcierto() {
		return fechaConcierto;
	}

	public void setFechaConcierto(LocalDate fechaConcierto) {
		this.fechaConcierto = fechaConcierto;
	}
	
	
	@Override
	public String toString() {
		return "Cantante [nombre=" +  nombre + ", cancion=" +  cancion + ", fechaConcierto=" +  fechaConcierto
				+ ""+ "]";
	}
}
