package programa;

import clases.GestorCanciones;

public class Programa {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		System.out.println("1");
		System.out.println("Creamos el gestor");
		GestorCanciones gestor = new GestorCanciones();
		System.out.println("2");
		System.out.println("Damos de alta cantantes");
		gestor.altaCantante("Lil Uzi Vert","P2");
		gestor.altaCantante("J Balvin","COMO UN BEBE");
		gestor.altaCantante("Joji", "Will He");
		System.out.println("3");
		System.out.println("Listamos cantantes");
		gestor.listarCantantes();
		System.out.println("4");
		System.out.println("Buscamos cantante Joji");
		System.out.println(gestor.buscarCantante("Joji"));
		System.out.println("5");
		System.out.println("Damos de alta canciones");
		gestor.altaCancion("P2", "Eternal Atake", 3.54, "2020-03-06");
		gestor.altaCancion("COMO UN BEB�", "OASIS", 3.38, "2019-06-28");
		gestor.altaCancion("Will He", "In Tongues", 3.22, "2017-11-03");
		System.out.println("Listamos Canciones");
		gestor.listarCanciones();
		System.out.println("6");
		System.out.println("Asignar Cantantes");
		gestor.asignarCantante("Lil Uzi Vert", "P2");
		gestor.asignarCantante("Joji", "Will He");
		System.out.println("7");
		System.out.println("Mostrar cancion de cantante Lil Uzi Vert");
		gestor.listarCancionesCantantes("Lil Uzi Vert");
		System.out.println("Mostrar cancion de cantante J Balvin");
		gestor.listarCancionesCantantes("J Balvin");
		System.out.println("8");
		System.out.println("Mostrar canciones por a�o");
		gestor.listarCancionAnio(2019);
		System.out.println("9");
		System.out.println("Eliminar Cancion P2");
		gestor.eliminarCancion("P2");
		System.out.println("Listamos canciones");
		gestor.listarCanciones();
		System.out.println("10");
		System.out.println("Listamos cancion de Lil Uzi Vert ");
		gestor.listarCancionesCantantes("Lil Uzi Vert");
		System.out.println("Listamos cancion de J Balvin");
		gestor.listarCancionesCantantes("J Balvin");
		System.out.println("11");
		System.out.println("Listar cantantes antes del ere");
		gestor.listarCantantes();
		gestor.ere();
		System.out.println("Listar cantantes despues del ere");
		gestor.listarCantantes();
	}

}
